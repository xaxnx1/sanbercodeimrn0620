/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  constructor(subject,points,email){
    this.subjet1 = subject;
    this.points1 = [points]
    this.email1 = email
  }
  average(points){
  var total = 0;
  for(var i = 0; i < points.length; i++) {
    total += points[i];
  }
  var avg = total / points.length;
  return avg
  }
}
newscore = new Score("quiz-1",15,"bondra@gmail.com");
console.log(newscore.subjet1)
console.log(newscore.average([15,20]))
console.log(newscore.email1)

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
  for (var i = 1; i <=4; i++) {
    if (subject == "quiz-1"){
    console.log("{"+"email: "+data[i][0]+" subject: "+subject+" ,"+"points :"+data[i][1])}
    if (subject == "quiz-2"){
    console.log("{"+"email: "+data[i][0]+" subject: "+subject+" ,"+"points :"+data[i][2])}
    if (subject == "quiz-3"){
    console.log("{"+"email: "+data[i][0]+" subject: "+subject+" ,"+"points :"+data[i][3])}
  }
}

// TEST CASE
console.log(viewScores(data, "quiz-1"))
console.log(viewScores(data, "quiz-2"))
console.log(viewScores(data, "quiz-3"))

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

function recapScores(data) {
  var total = 0;
  for(var e = 1; e <= 4; e++) {
   var total2 = total + data[1][e];
}
var avg = total2 / 3;
  // var a = data[1][1]
  // var b = data[1][2]
  // var c = data[1][3]
  // total1 = a + b + c
  // var rata1 = total1/data[1].length
  for (var i = 1; i <= 4; i++) {
    console.log(i+". "+"Email: "+data[i][0]+"\n"+
      "Rata-rata: "+avg)
  }
}

console.log(recapScores(data));
