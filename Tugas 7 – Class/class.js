//nomor1
//release 0
class Animal {
    constructor(name){
    	this.name = name;
    	this.legs = 4;
    	this.cold_blooded = false;
    }
}

var sheep = new Animal("shaun");
console.log(sheep.name) 
console.log(sheep.legs) 
console.log(sheep.cold_blooded) 
//release 1
class Ape extends Animal{
	constructor(name,name1){
		super(name)
		this.name1 = name1;
		this.legs1 = 2;
	}
	yell(){
		return "Auooo"
	}
}
var sungokong = new Ape("kera sakti")
console.log(sungokong.name) 
console.log(sungokong.legs1) 
console.log(sungokong.cold_blooded) 
console.log(sungokong.yell()) 
 
 class Frog extends Ape{
 	constructor(name,name1){
 		super(name)
 	}
 	jump(){
 		return "hop hop"
 	}
 }
var kodok = new Frog("buduk")
console.log(kodok.name) 
console.log(sungokong.legs1) 
console.log(sungokong.cold_blooded) 
console.log(kodok.jump());
//nomor 2
// function Clock({ template }) {
  
//   var timer;

//   function render() {
//     var date = new Date();

//     var hours = date.getHours();
//     if (hours < 10) hours = '0' + hours;

//     var mins = date.getMinutes();
//     if (mins < 10) mins = '0' + mins;

//     var secs = date.getSeconds();
//     if (secs < 10) secs = '0' + secs;

//     var output = template
//       .replace('h', hours)
//       .replace('m', mins)
//       .replace('s', secs);

//     console.log(output);
//   }

//   this.stop = function() {
//     clearInterval(timer);
//   };

//   this.start = function() {
//     render();
//     timer = setInterval(render, 1000);
//   };

// }
class Clock {
constructor({ template}){     
  var timer;

  function render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  this.stop = function() {
    clearInterval(timer);
  };

  this.start = function() {
    render();
    timer = setInterval(render, 1000);
  };
}
}
var clock = new Clock({template: 'h:m:s'});
clock.start();  
