import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View,Platform,image,TouchableOpacity,Flatlist} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './videoitem.js';
import data from './data.json'
export default class App extends React.Component {
  render(){
  return (
    <View style={styles.container}>
        <View style={styles.navbar}>
          <image src={require('./images/logo.png')}/>
          <View style={styles.now}>
          <TouchableOpacity>
            <Icon style={styles.navItem} name="search" size={25}/>
          </TouchableOpacity>
          <TouchableOpacity>
            <Icon name="account-circle" size={25}/>
          </TouchableOpacity>
          </View>
        </View>
        <View style={styles.body}>
          <Flatlist
          data={data.items}
          renderItem={(video)=><videoitem video={video.item}/>}
          keyExtractor={(item)=>item.id}
          itemSeparatorComponent={()=><View style={{height:0.5,backgroundcolor:'#E5E5E5'}}/>}
           />
        </View>
        <View style={styles.tabbar}>
          <TouchableOpacity style={styles.tabitem}>
            <Icon name="home" size={25}/>
            <Text style={styles.tabtitle}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabitem}>
            <Icon name="whatshot" size={25}/>
            <Text style={styles.tabtitle}>Trending</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabitem}>
            <Icon name="subscriptions" size={25}/>
            <Text style={styles.tabtitle}>subscriptions</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabitem}>
            <Icon name="folder" size={25}/>
            <Text style={styles.tabtitle}>Library</Text>
          </TouchableOpacity>
        </View>
    </View>
  );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navbar: {
    height: 55,
    backgroundColor: 'grey',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignitems: 'center',
    justifyContent: 'space-between',
  },
  now: {
    flexDirection: 'row'
  },
  navItem: {
    marginleft: 25
  },
  tabbar: {
    height: 60,
    backgroundColor: 'grey',
    borderTopwidth: 0.5,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  body: {
    flex: 1
  },
  tabitem: {
    alignitems: 'center',
    justifyContent: 'center'
  },
  tabtitle: {
    fontSize: 11,
    color: '#3c3c3c',
    paddingTop: 3
  }
});
