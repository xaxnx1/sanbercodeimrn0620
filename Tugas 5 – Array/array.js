//nomor 1
function range(startNum, finishNum) {
	var ans = [];
	if (startNum + finishNum > 0) {
		for (var i = startNum; i <= finishNum; i++){
		ans.push(i)
		}	
		if(startNum > finishNum){
			for (var i = finishNum; i <= startNum; i++){
			ans.unshift(i)
			}
		}
	}
	else{
		return -1
	}
	return ans;
}

console.log(range(1,10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54,50))
console.log(range())
//nomor 2
console.log("")
function rangeWithStep(startNum1, finishNum1, step){
	var answer = [];
	if (startNum1 + finishNum1 > 0){
		for (var b = startNum1; b <= finishNum1 ; b = b + step) {
		answer.push(b)
		}	
	}

	if(startNum1 > finishNum1){
		for (var b = startNum1; b >= finishNum1; b = b - step){
			answer.push(b)
			}
		}
	return answer
}
console.log(rangeWithStep(1,10,2))
console.log(rangeWithStep(11,23,3))
console.log(rangeWithStep(5,2,1))
console.log(rangeWithStep(29,2,4))
//nomor 3
console.log("")
function sum(startNum2, finishNum2, step1) {
	if (typeof(step1)==='undefined'){
		return 1;
	}
	var answer1 = [];
	for (var b = startNum2; b <= finishNum2 ; b = b + step1) {
		answer1.push(b)
		}
	if(startNum2>finishNum2){
		for (var b = finishNum2; b <= startNum2 ; b = b + step1) {
		answer1.push(b)
		}
	}	
	return answer1.reduce(function(a,b) {
		return a + b;
	}, 0)
}
console.log(sum(1,10,1))
console.log(sum(5,50,2))
console.log(sum(15,10,1))
console.log(sum(20,10,2))
console.log(sum(1))
console.log(sum())
//nomor 4
console.log("")
var input1 = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 

function dataHandiling(input) {
	for (var c = 0; c < 4; c++) {
	 	var start = input[c]
	 console.log("Nomor ID: "+start[0]+"\n"+"Nama Lengkap: "+start[1]+"\n"+"TTL: "+start[2]+" "+start[3]+"\n"+"Hobi: "+start[4]+"\n")
	}	
		
}
console.log(dataHandiling(input1))
//nomor 5
console.log("")
function balikKata(words) {
	var newString = "";
	for (var d = words.length - 1; d >=0 ; d--) {
	 	newString += words[d]
	 }
	 return newString;

}
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah")) 
console.log(balikKata("racecar")) 
console.log(balikKata("I am Sanbers"))
//nomor 6
console.log("")
var input3 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
function dataHandling2(input2) {
	input2.splice(1,1,"Roman Alamsyah Elsharawy")
	input2.splice(2,1,"Provinsi Bandar Lampung")
	input2.splice(4,0,"Pria","SMA Internasional Metro")
	const sp = input2[3].split("/")
	switch(input2){
		case '21/01/1989':
		console.log("januari")
		break;
		case '21/02/1989':
		console.log("febuari")
		break;
		case '21/03/1989':
		console.log("maret")
		break;
		case '21/04/1989':
		console.log("april")
		break;
		case '21/05/1989':
		console.log ("mei")
		break;
		case '06':
		console.log("juni")
		break;
		case '07':
		console.log("juli")
		break;
		case '08':
		console.log("agustus")
		break;
		case '09':
		console.log("september")
		break;
		case '10':
		console.log("october")
		break;
		case '11':
		console.log("november")
		break;
		case '12':
		console.log("desember")
		break;
	}

	return input2
}
console.log(dataHandling2(input3))